class calc:
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def get_sum(self):
        return self.a + self.b
    
    def get_diff(self):
        return self.a-self.b
    
    def get_prod(self):
        return self.a*self.b