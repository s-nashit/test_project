import unittest
from main import calc

class test_case(unittest.TestCase):

    def test_sum(self):
        calculation = calc(18, 2)
        self.assertEqual(calculation.get_sum(), 20, 'The sum is wrong.')

    def test_diff(self):
        calculation = calc(8,2)
        self.assertLess(calculation.get_diff(), 15, 'the difference is wrong.')

    def test_prod(self):
        calculation = calc(8,12)
        self.assertEqual(calculation.get_prod(), 96, 'the product is wrong.')

if __name__ == '__main__':
    unittest.main()